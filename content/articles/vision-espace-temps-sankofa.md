+++
title = "Vision Espace Temps Sankofa"
slug = "vision-espace-temps-sankofa"
language = "fr"
themes = ["Society"]
date_published = "2020-04-16"
image = "/media/uploads/dscf0706.jpg"
image_caption = "photo Adelino Duarte, traversée vers Santo Antao"

[[links]]
link = "[Afrotopia](https://www.cairn.info/revue-afrique-contemporaine-2016-1-page-150.htm), Felwin Sarr (revue de CAIRN.info)"

[[links]]
link = " Réflexion de Malcom Ferdinand [](https://soundcloud.com/oliwon-lakarayib/episode-2-penser-lecologie-depuis-le-monde-caribeen-une-ecologie-decoloniale)sur soundcloud \"[Penser l'écologie depuis le monde caribéen, une écologie décoloniale](https://soundcloud.com/oliwon-lakarayib/episode-2-penser-lecologie-depuis-le-monde-caribeen-une-ecologie-decoloniale)\""

[[links]]
link = "Francoise Vergès sur France Culture en 2019, [\"La France s’est construite sur son image d’empire colonial\"](https://youtu.be/26bOApXSlp4) (Youtube)"

[[links]]
link = "Site internet de [Faith Rinngold](https://www.faithringgold.com/) pour découvrir ses œuvres."

[[links]]
link = "[Ozon Cyclery Berlin](https://ozoncyclery.com/) pour les vélos en bambou"
+++
## Une vision

*Vision distante, floue mais visible? Invisible que je ne vois pas. Visible que je ne veux pas voir…*

Cet article de blog devait initialement traiter de façon légère le sujet des comportements individuels au quotidien qui permettent de minimiser l'impact sur l’environnement et maximiser l’impact social: une "green attitude" écologique et éthique. 

Une vision très "occidentale" pas du tout adaptée à la réalité du monde, je l'avoue.

Si des hommes sont capables d'agir de façon irresponsable au point de détruire la planète (lui mettant la fièvre et ravageant ses poumons), alors ils seraient capables de se comporter d’une toute autre manière pour la laisser respirer, ce qui signifierait un meilleur bien-être pour tous vivants sur le globe et pour les générations futures.

Et puis, vint ce virus, invisible. Écrivant mon article, ce concept naïf et fantaisiste de "green attitude" ne me plaît guère. J'y préfère la notion plus percutante et sans équivoque de "comportement responsable et juste". Ce qui pose la question de qui est responsable de la crise écologique et qui sont les victimes.

Cette pandémie résonne comme un énième signal d’alarme. Le percevons nous ou bien on ferme les yeux comme d'habitude? Cette période de confinement est propice à la réflexion et au questionnement des valeurs dominantes actuelles de notre société.

![Photo prise à Berlin, le monument "Der Rufer" (le crieur)](/media/uploads/rufernew.jpeg)

Recroquevillé dans notre mondialisation, nous ne regardons pas le monde. Ainsi, dégustant notre café ou ce délicieux chocolat, notre perspective eurocentrée (Françoise Vergès sur la pensée coloniale et le féminisme) refuse de voir les souffrances subies par ceux qui produisent nos petits plaisirs quotidiens. 

Il serait malvenu pour moi de parler "green attitude" en respect pour les peuples autochtones, les personnes exploitées en Afrique et en Asie ou bien même, plus proche de nous, les familles pauvres des pays riches occidentaux (où on s'aperçoit que les activités essentielles des pays, en cette période de crise sanitaire, sont soutenues par des emplois sous payés occupés à 80% par des femmes, les plus exposées au risque du virus).

Et puis, voit-on la violence faite aux activistes écologiques des pays du Sud? Des membres des peuples autochtones "gardiens des forêts" et défenseurs de l'environnement sont assassinés en Amazonie!

Si l’homme respecte la Nature, alors de facto il prend soin de lui même. Ici, l'écologique devrait même être le prolongement de la question sociale. 

Être tolérant envers l’Autre tout simplement. Une acceptation totale de la différence et de la diversité, un respect de l'autre individu quelque soit son genre, son orientation sexuelle, sa culture, sa couleur, sa position sociale. Lui laisser sa place dans le monde, à la forêt et à l'Autre.

![Souvenirs d'Éthiopie, un magnifique pays. Si beau, si différent.](/media/uploads/ethionew.jpeg)

Une posture égoïste qui se traduit par une désagrégation: certains hommes se détachent au dessus de la Nature. Cette dangereuse rupture avec la Nature, justifiée par une supposée hiérarchisation des espèces (théories validées en Europe au XVIII siècle) où l'homme serait au sommet de la pyramide, lui donne le droit de détruire son environnement. Tout aussi répugnant, ce système de pensée sert de justification pour classifier les êtres humains et traiter certains comme des sous-hommes, juste des corps, leur ôtant toute âme et dignité.

## Un espace

*Distance, geste de protection pour nos personnes vulnérables et notre écosystème fragile.*

Le coronavirus impose à l'espèce humaine d’opérer une distanciation sociale contre nature. Un comportement collectif pour ralentir la contagion, protéger et sauver des vies en évitant de surcharger nos centres hospitaliers sous-équipés. Nous éviter physiquement, temporairement, tel est notre seul geste de solidarité ultime et de survie.

Une parenthèse temporelle: les colons européens qui arrivèrent (et non pas découvrirent) dans les Amériques à partir de 1492 avec Christophe Colomb, ramenèrent aussi leurs virus, contre lesquels les populations indigènes n'étaient pas immunisées. La pandémie ravagea une très grande partie de la population amérindienne.

Retour au présent. Le virus nous dit qu’il faut respecter une certaine distanciation avec la Nature. Les spécialistes nous avertissaient pourtant: nous ne respectons plus la barrière des espèces. L'homme s’expose à de nouveaux virus provenant d’animaux sauvages. Ces pauvres animaux, en voie d'extinction, sont eux mêmes victimes de la cupidité d'individus qui pratiquent des trafics illégaux. 

La forêt primaire doit rester sauvage et préservée de toute intrusion humaine. Les peuples autochtones sont les garants des forêts car ils ont une compréhension des limites grâce à leurs traditions ancestrales.

Petit break artistique avec cet ange:

![London, une rue à Camden. Amy Winehouse, une voix qui traverse l’espace](/media/uploads/img_4498-1-.jpg)

<iframe width="560" height="315" src="https://www.youtube.com/embed/bjNLCbIMzZs" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Malheureusement, l’urbanisation galopante et l’agriculture industrielle, eux n'ont pas de limites et exigent toujours plus de surface. L'agriculture intensive sert principalement l'élevage bovin et le marché de l'alimentation du bétail. 

On connaissait déjà l’impact désastreux de la déforestation sur le réchauffement climatique (perte d’absorption de CO2 par les arbres, extinctions d’espèces…). Nous en découvrons un autre dévastateur, sanitaire cette fois ci. Moi perso, j'arrête la consommation de viande.

### moi dans tout ça

On entend souvent dire que le consommateur a un rôle important. Mais est ce que le chemin vers une société plus respectueuse de l’Homme et de la Nature doit forcément passer par le seul critère de la consommation? Lorsque l'on sait que nos ressources ne sont pas infinies et donc épuisables? Et puis, pensons à tous ces gens qui n’ont pas les moyens financiers nécessaires pour pouvoir jouer ce rôle de régulateur (quid des inégalités sociales).

*être “moins consommateur passif” et plus citoyen actif*

Consommer responsable est un très bon début, chacun à son rythme. Consommer moins, consommer autrement, consommer mieux en matière d’énergie, transport et alimentation. L'intérêt porte alors sur l’alimentation bio, le commerce local et éthique, l'efficacité et sobriété énergétique, énergies renouvelables, la réduction de l’empreinte carbone concernant ma mobilité, la possibilité de privilégier le train…

![Le vélo, mon moyen de transport de prédilection à Berlin](/media/uploads/dscf2839.jpg)

Ces alternatives responsables mentionnées ci dessus contiennent ***une valeur ajoutée humaine et éthique très significative***. Elles ont un prix. Par exemple, l’agriculture paysanne bannit les pesticides polluants et créé des milliers d’emplois nouveaux, contrairement à un agrobusiness mono-culture d’exportation, polluant et robotisé. 

Il faut trouver des solutions pour que tous puissions avoir accès (distribution locale efficace et surtout hausse du pouvoir d’achat) à ces produits éthiques et sains.

![Le jardin de mon père. Il faut cultiver son jardin, c’est aussi une philosophie à Cabo Verde](/media/uploads/papanew.jpeg)

Revenons en au rôle du consommateur citoyen. Un rôle très limité car il y a de fortes oppositions à l’idée de changer les choses (le "TINA", There Is No Alternative). Nos sociétés (irresponsables) productivistes et de consommation ont besoin de travailleurs et de consommateurs, mais pas vraiment de citoyens responsables. 

Et puis, le néolibéralisme financier de notre époque est complètement fou, un casino virtuel dopé aux algorithmes recherchant le profit court terme (trading à la nanoseconde!) totalement déconnecté de l'économie productive de notre vraie vie qui se pense sur du long terme, durable.

### Nous, dans tout ça

Une ***bonne gouvernance*** (comme dirait Wangari Maathai dans son ouvrage "Challenges for Africa") joue un rôle important pour orienter une politique environnementale et sociale efficace (investissements massifs écologiques et justice fiscale). Une nouvelle société basée sur un équilibre entre activités écologiques locales et esprit d’ouverture, d’accueil et de coopération. 

Il faut une forte volonté politique de changement, capable de résister aux pressions des entreprises multinationales ou de l’extrême financiarisation de l’économie. Le consommateur seul est impuissant (c'est mon humble avis, les bonnes intentions du marché auto-régulateur, je n'y crois pas).

Parenthèse musicale avec ci dessous une photo de la superbe pochette d’album contenant le titre [“Hora ja tchega” de Franck Mimita & Conjunto](https://www.youtube.com/watch?v=2-cy9kwarjU)

![L'heure est venue](/media/uploads/horajacheganew.jpeg)

> *Africa ja corda* \
> *Nos força ja duplica* \
> *Negro ja grita vitoria* \
> *Medo ja trancas na corpe* \
> *Hora ja tchega* \
> *Africa ja corda* \
> *Medo ja ca tem* \
> *Pa no bem luta té fim* \
> *Nos vitoria ta na mon* \
> *Graças a nos irmon Cabral* \
> *Qui derrota colonialismo* \
> *Pa liberta se pove irmon*

De nos jours, les gouvernements sont plus attachés à une idéologie néo-libérale marchande qu’à l’authentique liberté individuelle et le bonheur d’une population.

***Liberté de circulation des biens et capitaux, les néo-libéraux applaudissent. Par contre, pour sauvegarder des réfugiés en mer, ils tournent le dos!*** 

La cruauté va jusqu’à criminaliser la solidarité envers les réfugiés sur terre comme en mer. Honte à "notre" "Europe forteresse"

### Temps… de décoloniser

Le monde d’après passera par une décolonisation aussi bien mentale (dans la manière de percevoir et penser) que physique (dans notre manière d’habiter le Monde). Habiter le Monde est, entre autre, un concept repris par Malcom Ferdinand dans son ouvrage ["Une écologie décoloniale](https://www.seuil.com/ouvrage/une-ecologie-decoloniale-malcom-ferdinand/9782021388497)" où il présente la *"double fracture de la modernité, fracture coloniale et environnementale".* 

***Décoloniser sa Vision, Décoloniser son Espace.***

Parenthèse temporelle: l'entreprise coloniale et son corollaire, un système capitaliste impérialiste, n'auraient pu se concrétiser sans le génocide sur les premiers peuples d'Amérique, sans l'esclavage à grande échelle et sans une exploitation massive de la Nature. Beaucoup semblent s'en satisfaire encore aujourd'hui et refuse de remettre en cause ce modèle de développement génocidaire. Mais nous on n'oublie pas comme dirait l'autre.

***Décoloniser l’Histoire, le facteur Temps: Sankofa,“Regarder le passé, pour bâtir le futur”***

![New York, "New Amsterdam", où les Hollandais vendaient aux enchères des esclaves qui devinrent servants dans les familles de NY](/media/uploads/nyafrika.jpeg)

Plusieurs siècles de colonisation laissent des traces visibles dans nos sociétés actuelles. La pensée coloniale aujourd’hui se traduit par des mots, des non-dits, mises sous silence et des actions; des humiliations, une invisibilisation (la crise sanitaire du chlordécone en Martinique) et des injustices. 

La pensée coloniale c'est concevoir qu'il y ait des personnes de seconde zone dans des zones "périphériques" où l'on peut se permettre de faire des expérimentations sur des populations et des éléments naturels (en Afrique, en forêt amazonienne, dans les banlieues, aux Antilles, au Vietnam, dans l'océan Pacifique…).

!["We came to America" de Faith Rinngold, expo "The Color Line" à Paris](/media/uploads/statueliberty.jpeg)

## tolérance et place pour tous

***Les êtres vivants humains SONT la Nature***, parmi un écosystème complexe. Observer la Nature du haut de notre prétention nous mène au désastre comme nous rappellent l'exploitation sans limite de l'environnement (colonisation de la terre) et la colonisation des hommes. 

Deux aberrations, deux injustices qui persistent dans notre monde "moderne", sous le même étendard d’un système nommé capitalisme.

Indéniablement, les questions de protection de l’environnement, justice sociale et lutte contre les discriminations sont interconnectées et convergent vers un même idéal de justice et de bien-être pour tous, dans tous les continents. 

Je pense que c’est une bonne conclusion pour penser "décolonial".

![Afrotopia de l’écrivain et économiste Felwine Sarr](/media/uploads/felwinecafe.jpeg)

*Créer son imaginaire pour (s’) inspirer.* Dédié à mon tonton Géraldo. 

*Adelino Duarte, le 16 avril 2020*