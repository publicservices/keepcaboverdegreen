+++
title = "Ailton Krenak"
serie = "que-des-numeros-6-dans-ma-team"
slug = "ailton-krenak"
language = "fr"
themes = ["Inspirations"]
date_published = "2021-02-18"
image = "/media/uploads/external-content.duckduckgo.com.jpg"
image_caption = "Ailton Krenak"

[[links]]
link = "Vidéo YouTube Le Monde Diplomatique Brésil (en portugais) sur Ailton Krenak: [Vozes da Floresta](https://youtu.be/KRTJIh1os4w)\n\n[](https://en.unesco.org/biosphere/lac/mata-atlantica)\n\n[](https://en.unesco.org/biosphere/lac/mata-atlantica)\n\n[](https://www.francetvinfo.fr/economie/emploi/metiers/agriculture/bresil-les-ecologistes-frequemment-assassines-en-amazonie_3452169.html)\n\n[](https://www.bastamag.net/Amazonie-incendies-deforestation-Bolsonaro-soja-boeufs-fazendeiros-corruption-peuples-autochtones)\n\n[](https://blogs.mediapart.fr/edition/la-mort-est-dans-le-pre/article/211015/les-degats-environnementaux-de-lagriculture-intensive)\n\n[](https://www.globalwitness.org/en/campaigns/forests/beef-banks-and-brazilian-amazon/)\n\n[](https://www.amnesty.fr/actualites/bresil-jbs-elevage-illegal-de-betail-en-amazonie)"

[[links]]
link = "Site officiel de [Survival international](https://www.survivalinternational.fr/) organisation protectrice des droits des peuples indigènes.\n\n[](https://webdoc.france24.com/bresil-barrage-mariana/)"

[[links]]
link = "Vidéo YouTube TEDx en portugais \"[Demarcar terras indígenas é honrar nossos ancestrais](https://youtu.be/ZxC1IyunlFc)\" par Joziléia Daniza Kaingang"

[[links]]
link = "Émission radio RFI avec l'invité Malcom Ferdinand: [colonisation et esclavage ont conditionné la crise écologique actuelle ](https://www.rfi.fr/fr/podcasts/c-est-pas-du-vent/20210107-la-colonisation-et-l-esclavage-ont-conditionn%C3%A9-la-crise-%C3%A9cologique-actuelle)"

[[links]]
link = "Émission radio RFI avec l'invité Guillaume Blanc:[ invention du colonialisme vert, pour en finir avec le mythe de l'Eden africain](https://www.rfi.fr/fr/podcasts/20201011-guillaume-blanc-invention-colonialisme-vert-en-finir-le-mythe-eden-africain) où on découvre le rôle néfaste de l'association WWF"

[[links]]
link = "[La Mata atlantica](https://en.unesco.org/biosphere/lac/mata-atlantica) (forêt atlantique), site de l'UNESCO"

[[links]]
link = "Article de Géo: [Déforestation en 2020 en Amazonie](<1. Déforestation en 2020: article de [geo.fr](https://www.geo.fr/environnement/bresil-pres-de-8-500-km2-de-foret-amazonienne-deboises-en-2020-203381?fbclid=IwAR3UvUV-6je95dlihTeFZMZfQ3jmq51cfu83D6P3ZfbifGrJRYzBTWWhKws)>), 8500 km2 de forêt soit 1,2 millions de terrains de foot."

[[links]]
link = "PDF: [Barbarie moderne de l’agrobusiness au Brésil](https://www.cetri.be/IMG/pdf/bresil-2.pdf)"

[[links]]
link = "Article de l'Institut Goethe: [racisme au Brésil](https://www.goethe.de/prj/lat/fr/ide/21666569.html)"

[[links]]
link = "[Rôle du géant de la viande JBS](https://www.amnesty.fr/actualites/bresil-jbs-elevage-illegal-de-betail-en-amazonie) dans les violations de droits humains en Amazonie, par Amnesty International"

[[links]]
link = "[La catastrophe miniere Mariana](https://webdoc.france24.com/bresil-barrage-mariana/) (reportage france 24)"

[[links]]
link = "[Beef, banks and brazilian Amazon](https://www.globalwitness.org/en/campaigns/forests/beef-banks-and-brazilian-amazon/): le rôle de la production de boeuf dans la déforestation"

[[links]]
link = "[Les dégâts environnementaux de l'agriculture intensive](https://blogs.mediapart.fr/edition/la-mort-est-dans-le-pre/article/211015/les-degats-environnementaux-de-lagriculture-intensive), blog médiapart: les Français sont les premiers consommateurs européens de viande bovine"

[[links]]
link = "[Les écologistes fréquemment assassinés en Amazonie](https://www.francetvinfo.fr/economie/emploi/metiers/agriculture/bresil-les-ecologistes-frequemment-assassines-en-amazonie_3452169.html) (france info)"

[[links]]
link = "[Les pyromanes de l'Amazonie brésilienne](https://www.bastamag.net/Amazonie-incendies-deforestation-Bolsonaro-soja-boeufs-fazendeiros-corruption-peuples-autochtones), article de bastamag: “*Le Brésil compte plus de 215 millions de têtes de bétail, dont 40% paissent désormais en Amazonie, sur des terres déboisées. Le nombre de bêtes broutant les anciennes forêts primaires désormais transformées en pâturages a quasiment doublé en vingt ans.*”"
+++
Après l'épisode 1 de "Que des numéros 6 dans ma team" consacré à Bill Russell, on continue notre tour d'horizon consacré aux défenseurs. Défenseurs de la forêt et de l’environnement, défenseurs des droits des nations indigènes. 

En 2016, une protestation pacifique contre la construction d'un pipeline aux USA (Dakota) sera un fait marquant dans ma prise de conscience écologique et encore une fois révélatrice des abus des droits de l'Homme que subissent les populations indigènes. La répression violente envers les manifestants par une police hyper-militarisée au service d'une multinationale pétrolière me sidère.

En 2020, un jugement fédéral bloque la construction du Dakota pipeline et exige une étude d'impact écologique. Une victoire juridique même si le combat n'est pas encore fini. En 2021, le nouveau Président Biden révoque le permis de construire du pipeline Keystone XL reliant le Canada. Ces bonnes nouvelles ne concernent pas seulement les peuples indigènes mais nous tous.

![Photo de Dallas Goldtooth](/media/uploads/sept-4th-500-folks-after-sacred-site-bulldozing_396_orig.jpg)

La situation est toutefois très alarmante à l'heure actuelle en Amérique du Sud et au Brésil.

Cette note de blog est d'ailleurs consacrée à une voix importante au Brésil, celle de Ailton Krenak. Né en 1954, défenseur des droits des peuples indigènes, environnementaliste et écrivain, il obtint en 2020 le prix de l'intellectuel de l'année, décerné par l'Union Brésilienne des écrivains.

Je me dois aussi d'honorer d'autres compagnons de lutte qu'il cite: Chico Mendes, Joenia Wapixana, Marçal de Souza (Tupa-i), Davi Kopenawa, le Chef Raoni…Je le ferai dans ma prochaine note de blog.

## La Fuite

Un épisode tragique interrompant une enfance joyeuse passée dans la vallée du Rio Doce, au bord du fleuve. Un exile dans son propre pays car Ailton est rejeté dans son propre pays.

Dans les années 70, règne au Brésil une dictature militaire violente (1964-1985): elle intimide, expulse, torture, tue des membres du peuple indigène au nom de "réformes et progrès du pays". L'enjeu de l'État est l'accaparation de la forêt, des terres indigènes pour déforester, réaliser de grands projets (routes, mines d'or et barrages), développer les grandes exploitations agrobusiness dévoreuses de surface pour toujours plus d'élevage bovin.

En 1973, Chico Buarque et Milton Nascimento dénoncent la censure et la violence de la dictature avec la chanson "Calice" jeu de mot pour "tais toi":

<iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/152816766&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe>

## L'Union

Ailton s'alphabétise à l'âge de 17 ans dans le monde des "civilisés". Il découvre le discours et les préjugés du "blanc" envers sa communauté avec des commentaires comme "une société arriérée", "des gens en état d’évolution", "feignants incapables qu’il faut assister" et "en voie d’extinction". Comme si les peuples premiers étaient une espèce animale. Un discours humiliant de la part de la classe politique qui a pour but de justifier la négation des droits des autochtones.

C'est paradoxalement le régime dictatorial qui annonce "l’émancipation des peuples indigènes qui sont des brésiliens à part entière". Mais derrière ce discours se cache la volonté sournoise de la dictature d'assimiler ces peuples, c'est à dire leur ôter leur identité pour finalement plus facilement les déloger de leurs terres très convoitées. Les autochtones ont vite compris la manipulation.

Ailton prend conscience de la nécessité de s'organiser pour résister. En parcourant l'immense pays pour rencontrer les autres groupes indigènes (on compte aujourd'hui plus de 300 groupes différents représentant 900 000 personnes au Brésil), il participe avec d'autres leaders à la formation de l'Union des Nations Indigènes.

*L'identité indigène est fondamentalement ancrée dans le lien des peuples avec leur terre, cela se traduit par une communion profonde avec la nature (terre, rivière et montagne sont sacrées et ont une mémoire).*

Leur revendication première est une demande de démarcation de leurs terres ancestrales. Il s'agit d'un droit historique, traditionnel car effectivement ces terres appartiennent et furent maintenues par les peuples indigènes avant même l'arrivée du premier colon portugais.

## Ailton et le jenipapo

En 1987, Ailton Krenak est invité à faire un discours devant l'assemblée constituante (c'est la fin de la dictature). Grandement inspiré, il va appliquer sur son visage la peinture traditionnelle jenipapo (le fruit qui tâche) pendant qu'il délivre son message. Son discours exigeant plus de respect et reconnaissance oblige la constitution de 1988 à inclure un chapitre consacré aux "indiens" (indios), réaffirmant leurs droits, coutumes et leur autonomie avec l'article 231 énonçant spécifiquement le droit historique sur leurs terres.

![Ailton Krenak, 34 ans, assemblée constituante, le 4 septembre 1987](/media/uploads/index.jpg)

## Réflexions et pensées

Ses livres, conférences et interviews sur internet sont captivants. Voici quelques thèmes qui m'ont plus.

## Relations

Contrairement aux blancs, l'indigène ne se pense pas séparé de la Nature, cela est inconcevable. Le concept selon lequel nous aurions d'un côté (plutôt, au dessus de la pyramide) l'homme et de l'autre l’environnement, est typiquement une théorie européenne.

Dans la relation vécue par le peuple indigène, on observe sacralisation, personnalisation et parentalité: la Terre est la Mère, la rivière a un nom (chez le peuple Krenak, le fleuve sacré s'appelle “ouatou”), la montagne est le grand-père qui communique et donne conseil. 

Dans un écosystème où règne une telle relation de respect, l'environnement se régénère.

## Sur la rencontre des deux mondes

Il y a 500 ans, lors de la rencontre des deux mondes sur les côtes brésiliennes, les peuples indigènes se posent des questions sur l’*être* du visiteur, ses coutumes et croyances. Alors que le colonisateur, en bon commerçant, s'intéresse à l’*avoir* de l’autre.

Le colon a une obsession: marchandiser la Nature, la réduire à une ressource exploitable via un extractivisme sans limite. Le colon va non seulement exploiter la Nature mais aussi les corps: l'indigène devient chose esclave, à la fois combustible et moteur d'une monoculture intensive dans les plantations, enrichissant certains en Europe (cf. les travaux de Malcom Ferdinand que je cite plus bas).

## Sur qui nous sommes et la force du Rêve

Aliéné, nous serions incapable de faire la différence entre “Je suis vivant” et “Je vis”. D'après Ailton, il faut avoir le courage d'être "radicalement vivant et ne pas négocier la survie". La vie n'a pas à être utile, elle est une danse cosmique et il faut garantir l'équilibre entre Terre et Ciel selon leur cosmologie. Il ne comprends pas que des gens puissent réduire la vie à une "simple chorégraphie utilitaire ridicule".

Le rêve étant une capacité de tout un chacun, transcendante, elle peut avoir une incidence sur notre quotidien lorsque nous la racontons à notre entourage et peut nous guider dans nos choix. Il croit en la force de l'imaginaire. Il faut que nous apprenions à rêver le monde que l'on veut voir et puis le concrétiser en actions.

> …reconnaître cette institution du rêve non pas comme le simple fait de dormir et rêver, mais comme un exercice discipliné pour rechercher dans le rêve des orientations nous guidant dans nos choix quotidiens.

## Colonisation et écologie

Dés la colonisation au Brésil en 1500, il y a une volonté de nier l'existence des peuples indigènes par la création d'un narratif délibérément erroné afin d'invisibiliser ces peuples ou en développant un ["racisme scientifique](https://www.goethe.de/prj/lat/fr/ide/21666569.html)".

La Mata Atlantica (la forêt atlantique) est décrite par les naturalistes européens du 19ème siècle comme une forêt vierge, louant ainsi le mythe d'un magnifique jardin d'Éden abandonné par Dieu sur terre. Mais Ailton souligne que cette forêt était en fait déjà habitée par les peuples Tupi et Guarani, les créateurs authentiques de ce jardin.

On découvre ainsi une colonialité dans une écologie qui cherche à effacer l'habitant traditionnel de son milieu naturel en l'ignorant et l'expulsant physiquement. Sur ce sujet, je vous invite à écouter l'environnementaliste et philosophe Malcom Ferdinand (émission radio RFI [C’est pas du vent](https://www.rfi.fr/fr/podcasts/c-est-pas-du-vent/20210107-la-colonisation-et-l-esclavage-ont-conditionn%C3%A9-la-crise-%C3%A9cologique-actuelle)), auteur de "Une écologie décoloniale".


## Sur la "fin du monde"

Dans son livre au titre provocateur "Idées pour retarder la fin du monde", Ailton exhorte en fait à une prise de conscience collective pour construire le monde d'après. Il serait bon de ralentir, "marcher légèrement sur Terre / pisar divagar aqui na Terra".

Il rappelle qu'il y a 500 ans, les indigènes ont été confrontés à leur propre fin du monde. Colonisation, virus et bactéries venus d'Europe et puis la violence du processus de "civilisation" ont exterminé une grande partie de la population indigène. On estime un génocide de 12 millions d'amérindiens ou 90% de la population fut décimée durant le premier siècle de colonisation de 1500 à 1600. 

Bien entendu, les peuples indigènes ont résisté et ils sont encore là aujourd’hui à résister.

A vrai dire, ce ne fut pas une "fin" du monde, ce fut en réalité une "guerre" des mondes et elle perdure encore aujourd’hui avec les assassinats d’autochtones et le mépris et l’hostilité de l'actuel gouvernement brésilien.

## Sur l'humanité

Le club de l'humanité, promesse eurocentrée, est en fait un club restreint, exclusif. En sa périphérie, se trouvent le "Tiers monde", les pays "en voie de développement". Basé uniquement sur un modèle d’homogénéisation soutenu par de superstructures mondiales centralisées. Ce club décide à sa guise de l'intégration des soit disant "retardataires" dans son monde "civilisé".

Aujourd’hui, nous savons qu'une telle perspective limitée plante une seule et fade "monoculture des idées" et soumet une seule forme d’organisation adaptée au seul besoin du marché. Une idéologie qui met en danger toutes les autres formes de vie et formes de vivre.

*L’humanité c’est la Terre dans toute sa diversité. Un monde de pluralités d’idées et de formes d’organisation.*

Pause musicale avec la chanteuse Kaê Guajajara, sur le thème de la Terre Mère et la connexion avec la terre ancestrale :

<iframe width="560" height="315" src="https://www.youtube.com/embed/szzDJahvUS8" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Écologie du désastre

Outre le fléau actuel du covid19 qui touche les peuples indigènes plus fragilisés, les populations des forêts sont confrontées à d'autres problèmes sanitaires: l'empoisonnement des rivières par le toxique mercure ou cyanure utilisés dans les mines d’or, par la rupture de barrages miniers contenant produits toxiques comme la catastrophe minière de Mariana de 2015 qui a coûté la vie à 272 personnes.

Ailton nous fait remarquer que lorsqu'une catastrophe a lieu, la solution facile privilégiée est le versement d'une indemnité financière. La société Vale responsable de la catastrophe minière Mariana a payé plus de 5 milliards d'euros à l'État du Minas Gerais pour éviter la Justice. 

Ainsi, une ***écologie du désastre*** est celle qui s'intègre à l’économie du désastre. L'écologie du désastre semble parfaitement accepter de coexister avec ce modèle économique qui commet crimes écologiques et violations de droits humains.

## Espoirs, Demarcação Já!

Il y a espoir car les actions de solidarité avec les peuples indigènes d'Amérique latine et dans le monde entier sont de plus en plus nombreuses et leurs voix résonnent de pus en plus fort.

Au Brésil, le Chef Raoni appelle à l'unité pour lutter contre la déforestation en affirmant que l'Amazonie n'est pas un problème brésilien mais bien une question universelle qui nous concerne tous.

Affirmer notre respect et notre reconnaissance des peuples indigènes, rendre visible leur lutte, comprendre et partager leur culture, privilégier une consommation éthique, refuser les accords commerciaux internationaux irresponsables (dont le traité entre l’Union européenne et le MERCOSUR) sont sans doute des pistes de réflexions et espaces d'actions.

***Comme nous le suggère Ailton Krenak, rêvons, mettons en pratique nos rêves et connectons nous, pour un retour dans les bras de notre Terre Mère.***

Adelino Duarte, le 18 février 2021
