+++
title = "Bill Russell"
serie = "que-des-numeros-6-dans-ma-team"
slug = "bill-russell"
language = "fr"
themes = ["Inspirations"]
date_published = "2021-02-13"
image = "/media/uploads/rus0-013-russell-dm2928-1-.jpg"
image_caption = "Bill Russell, number 6"

[[links]]
link = "\n\n*Cette vidéo YouTube de Sébastien Ferreira fait un portrait sympa de Bill Russell, l’activiste et son combat contre le racisme: [ici](https://youtu.be/Kro8MGpYpGk) (6 min.)*"

[[links]]
link = "*Le discours écrit [\"I have a Dream\"](https://www.naacp.org/i-have-a-dream-speech-full-march-on-washington/) de Dr. Martin Luther King Jr. [](https://www.naacp.org/i-have-a-dream-speech-full-march-on-washington/)*"

[[links]]
link = "*Article de theundefeated.com [Bill Russell, activist for the ages](https://theundefeated.com/features/bill-russell-activist-for-the-ages/)*"

[[links]]
link = "*Vidéo [Bill Russell: My Life, My Way](https://youtu.be/PU8NwhRAFg8)* (documentaire d'une durée de une heure)"

[[links]]
link = "*Article “Le jour où [Bill Russell](https://www.basketusa.com/news/603132/le-jour-ou-bill-russell-a-fait-limpasse-sur-un-match-pour-protester-contre-le-racisme/) a fait l’impasse sur un match pour protester contre le racisme” de basketusa.com*"

[[links]]
link = "*Article [Bill Russell’s Lifelong Fight Against Racism](https://www.slamonline.com/the-magazine/bill-russells-lifelong-fight-against-racism/) de slamonline.com*"

[[links]]
link = "*L'histoire tragique de **[Emmett Till](https://youtu.be/SYiI7j6GW68)**, vidéo YouTube de 3 minutes [](https://youtu.be/SYiI7j6GW68)*"

[[links]]
link = "*Site bandcamp de Skyzoo pour l'album hip hop jazz [Milestones](https://mmg-skyzoo.bandcamp.com/album/milestones)*"

[[links]]
link = "*[Booba \"N°10\"](https://youtu.be/IZmCwtYk98A) french hip hop music vidéo, YouTube* [](https://youtu.be/IZmCwtYk98A)"
+++
Le rêve d'un monde d'après (en opposition avec un "retour à la normalité") et ce souhait pour plus de justice dans ce monde sont omniprésents dans mon logiciel de pensée. Dans cette quête continuelle d'ouverture et de rencontres, quelles sont mes espoirs et inspirations du moment?

Combatifs et dotés d'un sixième sens, leur surpassement du "moi" est au service du "nous". J'ai donc décidé de faire des petits portraits de grands défenseurs qui me fascinent. Quel titre donner à cette série de portraits? 

Que des numéros 10 dans ma team? Le titre rap efficace et énergisant de Booba est certes un hymne au leadership, le surpassement, la débrouille mais mes héros ont des qualités exceptionnelles additionnelles qui en font des...numéros 6!

Mon équipe de défenseurs est solidaire, élégante et imaginative. Je me rappelle que dans le top 3 des joueurs NBA de mon ami Patrick aux USA, il y avait Julius Erving le numéro 6 des Sixers et un certain Bill Russell. Me voila soudainement en train de visionner des vidéos, lire des articles et interviews sur Bill Russell, un défenseur des droits civiques et légendaire numéro 6 de l'équipe de basketball des Celtics. Mon premier portrait lui est dédié et j'en suis particulièrement fier car j'ai découvert un leader incroyable.

> I am a man who plays basketball\
> That's what I do\
> That's not what I am

## Légende du Basketball

Bill Russell est une Légende du Basketball avec ses 11 titres (record!) de NBA gagnés en 13 années de carrière chez les Boston Celtics (entre 1957 et 1969). 

Du haut de ses 2 mètres 06, il révolutionne l'art de défendre grâce à ses qualités athlétiques et mentales exceptionnelles et son approche intellectuelle du jeu. En 1957, le rookie âgé de 23 ans, tout juste vainqueur des JO de Melbourne, réalise deux actions cruciales en finale NBA contribuant au premier titre de son équipe.

![Bill le numéro 6 et Wilt Chamberlain](/media/uploads/ba8b818e717f26af26cee4817797deb9.jpg)

La dynastie des Celtics débute donc avec Bill Russell: 8 titres seront remportés à la suite (un record!). 1959 marque le début de la rivalité sportive avec son ami Wilt Chamberlain. 

Le cool et flamboyant Chamberlain fait le show offensif (il marque 100 points contre les New York Knicks en 1962!). Mais Bill, grâce à son mental et son esprit d'équipe, est plus décisif et gagne les trophées.

## Grand défenseur des droits civiques

A l'instar de son grand-père Jake, Bill (né le 12 février 1934 en Louisiane) ne garde pas le silence face aux injustices. Sa mère et son père le préservent du danger racial et l'encouragent à exceller dans l'adversité. Pour se remettre dans le contexte d'être un jeune noir à cette époque, rappelons nous du lynchage de *Emmett Till* (voir lien "7" en bas de page), 14 ans, en 1955, perpétré par des adeptes de la suprématie blanche dans le pays des lois racistes Jim Crow. 

De nos jours, le danger racial existe d'ailleurs toujours aux USA, cette fois ci les crimes sont souvent commis par la police.

Lorsqu'il a 12 ans, sa maman décède, et son père va s'occuper seul de lui et de son frère. Bill décrit son papa comme un héro "My Father was my Hero".

Album “Milestones” (2020) du chanteur hip hop new-yorkais Skyzoo. On reconnait la mélodie de "Song for my Father" (1964) du légendaire musicien jazz Horace Silver (dont le père est capverdien d'ailleurs):

<iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/832631233&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe>

Sensible aux problèmes dans son pays, Bill dit haut et fort ce qu'il pense de la ville de Boston très raciste: "I don’t play for Boston, I play for Celtics" / "Je ne joue pas pour Boston, je joue pour les Celtics".

Il vit l'expérience invraisemblable de jouer dans l'équipe la plus progressiste de l'époque, qui doit dans le même temps subir l'hostilité et l'intolérance de son propre public. La haine envers le meilleur joueur de la NBA sera telle que des individus vont sauvagement vandaliser sa demeure.

> The Boston Celtics was the first NBA team to start 5 black players. They were rewarded for being so progressive: with titles. But the city did not appreciate it.

Le 28 août 1963, alors couronné de 5 titres NBA, il est présent en tant que soutien lors de la célèbre Marche de Washington où Martin Luther King va délivrer son historique discours "I have a Dream" (voir lien "2" en bas de page). Invité à être présent sur le podium autour de MLK, Bill décline humblement et préfère une place au premier rang parmi l'audience.

![Discours de Martin Luther King Jr. en 1963](/media/uploads/mlkdream.jpg)

À la mort de MLK Jr. le 4 avril 1968, Bill et Wilt Chamberlain, dévastés, n'ont pas la tête à jouer au basketball. Mais à la suite d'un vote des joueurs, les équipes des Celtics et Philadelphia 76ers décident finalement de jouer le match qui se déroulera sans réelle motivation.

En 1967, lui et d'autres sportifs noir américains soutiennent Muhammad Ali qui refuse de servir pour la guerre du Vietnam. Bill est convaincu de la sincérité de Ali, objecteur de conscience. En vain, la justice américaine condamnera Ali qui se verra infliger, entre autre, une longue interdiction de boxer.

Comme Ali, et comme Martin Luther King Jr. d'ailleurs, Bill était haï par une partie de la société américaine considérant comme ennemi quiconque oserait émettre une critique envers le racisme structurel, l'impérialisme ou le militarisme des USA. Le FBI surveillait Bill et, dans ses fichiers internes, le décrit honteusement comme un "négro" arrogant qui refuse de signer des autographes à des enfants.

![3 Légendes: Bill Russell, Muhammad Ali et Lew Alcindor (Kareem Abdul Jabbar)](/media/uploads/sports-illustrated-legacy-awardjpg.jpg)

Encore aujourd'hui, Bill Russell et son légendaire rire, à 87 ans, est une figure de la défense des droits civiques US, porteur d'un message de tolérance et d'unité. Son engagement ne se limite pas qu'aux USA, il fut le premier joueur NBA à voyager en Afrique afin d'y installer des centres de formation pour les enfants passionnés de basketball.

"Je ne me suis jamais permis d'être une victime" dit il. Quand la "ligne du respect" est franchie par l'intolérance, un désir de justice nous signifie de ne pas garder le silence et d'agir. Bill Russell est incontestablement un exemple d'abnégation et d'altruisme.

Adelino Duarte, le 23 fevrier 2021