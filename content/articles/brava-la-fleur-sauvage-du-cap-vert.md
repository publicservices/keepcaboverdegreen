+++
title = "BRAVA, fleur sauvage du Cap-Vert"
serie = "cabo-verde"
slug = "brava-la-fleur-sauvage-du-cap-vert"
language = "fr"
themes = ["Travel"]
date_published = "2019-06-26"
image = "/media/uploads/dscf1576.jpg"
image_caption = "Fajã d'Agua"

[[links]]
link = "*Morna par Ildo Lobo:  [\"N'cria ser poeta\"](https://youtu.be/E0AbPzy5clQ)*"

[[links]]
link = "*[La vie et l’œuvre de Eugénio Tavares](http://www.eugeniotavares.org/index.html) (en portugais)*"
+++
Cet article est dédié à l'île de Brava "la isolada", petite île isolée située à l'extrême sud de l'archipel du Cap-Vert (Cabo Verde).

## Brava, la mystérieuse

Lors de mon séjour sur l'île de Fogo, je fus captivé par cette île montagne que j'aperçois au loin, encerclée par cette mer azure fréquentée par baleines et dauphins. Elle s'appelle Brava et m'accompagne, spectatrice de mes footings sur la plage de sable noire de "Fonti Bila" ou de nos matchs sur cette même plage, en compagnie de Cap-verdiens, Guinéens et Sénégalais passionnés de football. 

A la fin du match, on contemple la danse entre l'île montagne Brava et le soleil. Le spectacle offre un magnifique coucher de soleil digne d'un paysage de carte postal.

![L'île de Brava nous appelle](/media/uploads/dscf1403.jpg)

## Brava, petite sœur de l'île de Fogo

En 1680, l'île de Brava (l'île fut découverte en 1462 le jour de la Saint-Jean Baptiste mais contrairement à d'autres îles qui généralement portent le nom d'un Saint, elle fut baptisée "brava" en langue crioula c'est à dire la "sauvage") fut une terre de refuge pour une grande partie de la population de l'île voisine Fogo à la suite d'une éruption volcanique qui a rendu l'île de Fogo temporairement invivable. 

L'île "sauvage" et l'île "feu" ("fogo" en portugais) ont des liens très forts. En plus, elles se ressemblent puisque toutes deux sont encore sismiquement actives puisque des activités sismiques y sont régulièrement enregistrées.  

J'entends l'appel de Brava et décide enfin de lui rendre visite après tant de voyages à Cabo Verde sans y prêter attention. Nous sommes fin février 2019 et les conditions climatiques ont révolté la mer dont les vagues énormes couvrent complètement la plage de Fonti Bila et se fracassent sur les rochers et le port. Apparemment, Brava veut bien m'accueillir mais l'océan n'est pas de cet avis. Finalement, après quelques jours de patience, la mer se calme et la traversée en bateau, de Fogo vers Brava, peut avoir lieu. Une traversée toutefois quelque peu mouvementée (donc prévoir des cachets contre le mal de transport si possible). 

Vous pouvez contacter une agence de voyage comme Qualitur (voici son [tripadvisor](https://fr.tripadvisor.be/Attraction_Review-g1185367-d8368247-Reviews-Qualitur_Viagens_e_Turismo-Sao_Filipe_Fogo.html) et l'avis du [Petit futé](https://www.petitfute.com/v52054-sao-filipe/c1122-voyage-transports/c747-tours-operateurs/c1161-tour-operateur-specialise/720431-qualitur.html)) située à Fogo (ville de São Filipe) pour l'organisation de votre séjour sur l'île de Brava aussi bien que sur les autres îles du Cap-Vert d'ailleurs (réservations pour le transport en bateau, vols internes, hébergement, excursions et conseils sur les sites touristiques, restaurants et activités).

![Coucher de soleil (vue sur Brava à partir de Fonti Bila, Fogo)](/media/uploads/img_7542.jpg)

## L'île aux fleurs

La ville principale de Brava c'est la charmante "Nova Sintra" avec son architecture coloniale portugaise. La ville fut implantée dans les hauteurs pour se protéger des pirates et se trouve en fait sur un ancien cratère géant. 

Île la plus fraîche du Cap-Vert, Brava est toute verte en temps de pluie et est surnommée l'île aux fleurs car il y a tout simplement des fleurs partout sur l'île. On y trouve notamment l'hibiscus.

![La place centrale de Vila Nova Sintra](/media/uploads/dscf1424.jpg)

## Eugénio Tavares

La rue de la Cultura nous mène tout droit à la maison de l'illustre... 

Eugénio Tavares (18/10/1867-01/06/1930), où sa statue est présente. 

Cet enfant de Brava est un très grand Monsieur du Cap-Vert. L'homme avec la jolie moustache sur les billets de Banque, c'est lui!

Eugénio écrit des poèmes dés l'âge de 10 ans et se voit publier son premier poème quand il a 15 ans. Eugénio le journaliste créa le journal "A Voz de Cabo Verde" et lança en 1900 aux USA le premier journal de langue portugaise. 

Eugénio grand penseur et engagé politique, rêve d'un Cap-Vert autonome, éveille les consciences des Cap-verdiens exploités, oubliés en pleine période de famine et il critique les injustices du colonisateur portugais au point qu'il doit s'exiler quelques années aux États-Unis en 1900 pour échapper au régime portugais qui veut l'arrêter.  

![Statue de Eugénio Tavares devant sa maison](/media/uploads/dscf1472.jpg)

## La Morna

Qui dit Eugénio le poète et musicien, dit la Morna. 

Ce style musical est né sur l'île de Boavista au Cap-Vert puis se répands sur les autres îles. C'est toutefois Eugénio Tavares, à Brava, qui va donner à la Morna tout le style caractéristique de ce genre musical. Grâce au lyrisme et romantisme que Eugénio insuffle dans sa poésie, il embellit la Morna, aujourd'hui musique nationale au Cap-Vert et Patrimoine culturel de l'Humanité. 

Il faut aussi noter que ses Mornas sont écrites en langue crioula. Le choix de cette langue n'est pas anodine et représente un message politique fort porteur d'un impact social immense: le crioulo, langue du peuple colonisé et oublié, est une langue qui sait parfaitement être poétique et esthétique et donc transmettrice d'émotions universelles. 

Aujourd'hui, la langue crioula fait partie de l'identité Cap-verdienne. 

De plus, le natif de Brava aborde avec mélancolie les thèmes de l'amour et la mer. *Il développe donc les thèmes de la Sodade et de la Partida, qui font partie du quotidien de chaque Cap-verdiennes et Cap-verdiens.* 

Extrait des paroles de la morna "Força de cretcheu" de Eugénio Tavares:

> *Ca tem nada na es vida*\
> *Mas grande que Amor*\
> *Se Deus ca tem medida*\
> *Amor inda é maior*\
> *Amor inda é maior*\
> *Maior que Mar, que Céu*\
> *Mas, entre tudo cretcheu*\
> *De meu inda é maior...*

Dans cette Morna, Eugénio Tavares parle d'un Amour plus fort que Dieu, plus fort que la Mer et le Ciel, l'Amour de sa cretcheu (son amoureuse à lui).   

Je vous conseille cette jolie Morna "Força de cretcheu" reprise par la chanteuse Gardenia:

<iframe width="560" height="315" src="https://www.youtube.com/embed/9L8qk0PQrZA" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Émigration vers América

La "Partida" c'est l'émigration, phénomène qui définit les îles du Cap-Vert où les conditions climatiques obligent les habitants à trouver une vie meilleure à l'étranger. 

A Brava, l'émigration débute au 18ème siècle et sera facilitée par l'arrivée de baleiniers sur le port de Fajã d'Agua. De nombreux hommes de Brava sont recrutés comme marins. Ces navires nord-américains pêchaient la baleine jusque les Açores (une pêche de la baleine pour transformation de sa chaire en huile pour les besoins de la révolution industrielle). Les marins cap-verdiens s'installeront petit à petit dans le Massachusetts et aident la famille restée à Brava. 

C'est ainsi que Brava a une "American touch" très prononcée.

## Le village de pêche Fajã d'Agua

La randonnée de Vila Nova Sintra vers Fajã d'Agua est fortement recommandée. La vue sur la baie est à couper le souffle. Une fois arrivé dans ce petit village de pêche, on a la sensation d'être dans un autre monde où le temps s'est soudainement arrêté. 

Bercé par la mélodie des vagues, la quiétude est totale. Vous pouvez aussi vous baigner à la piscine naturelle non loin. 

Au bout du village, on aperçoit l'ancienne piste d'aéroport, puis notre regard se pose sur un vaste océan infini. Pas tout à fait infini car de l'autre côté de l'atlantique, c'est l'Amérique et le Brésil. 

*Fajã d'Agua est en tout point fascinant. Ce lieu merveilleux et mystérieux est géographiquement et historiquement symbolique.* 

![La première vue sur la baie de Fajã d'Agua](/media/uploads/dscf1521.jpg)

La migration a aussi son lot de malheur, souvent évoqué dans les Mornas. Ici, un monument nous rappelle la tragédie de 1943 lorsque le navire "Matilde" parti de Fajã d'Agua en direction de l'Amérique, n'est jamais arrivé à destination. Il y avait plus de 50 membres à bord.

![Hommage aux disparus en mer](/media/uploads/dscf1528.jpg)

## Ja m'konché Brava, la isolada

Comme le chante le groupe capverdien Splash! dans la magnifique chanson "Izolada": j'ai fait la connaissance d'une "isolée". L'île de Brava, elle, c'est sûr, transmet vraiment beaucoup d'amour et de tendresse. 

<iframe width="560" height="315" src="https://www.youtube.com/embed/8gOkU07OaPU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Merci Brava pour ton accueil et ta beauté, tu es un havre de paix. Une destination idéale pour les amoureuses et amoureux de Montagne et Nature et qui recherchent un lieu paisible et le contact avec une population humble et accueillante. 

Destination parfaite pour se ressourcer ou pour rechercher l'inspiration en suivant, pourquoi pas, les traces du "Camões de Cabo Verde", Eugénio Tavares. A bientôt pour de nouvelles aventures!

Adelino Duarte, le 26 Juin 2019.