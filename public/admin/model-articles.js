import title from './field-title.js'
import slug from './field-slug.js'
import draft from './field-draft.js'
import body from './field-body.js'
import language from './field-article-language.js'
import datePublished from './field-date-published.js'
import featuredImage from './field-featured-image.js'

const articles  = {
    name: 'articles',
    label: 'Articles',
    label_singular: 'Article',
    folder: 'content/articles',
    format: 'toml-frontmatter',
    create: true,
    slug: '{{title}}',
    editor: {
	preview: true
    },
    fields: [
	title,
	slug,
	draft,
	language,
	datePublished,
	featuredImage,
	body
    ]
}

export default articles
