import articles from './model-articles.js'
import pages from './model-pages.js'
import configs from './model-configs.js'

export default [
    articles,
    pages,
    configs
]
