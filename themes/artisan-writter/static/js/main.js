import 'https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js'
import ImagesLoaded from './images-loaded.js'
import FontsLoaded from './fonts-loaded.js'

customElements.define('fonts-loaded', FontsLoaded)
customElements.define('images-loaded', ImagesLoaded)

const documentReady = async () => {
	if (document.readyState === 'complete') {
		console.log('Keep Cabo Verde Green!')
		const $body = document.querySelector('body')
		$body.classList.remove('is-noJs')

		loadFonts()
		loadImages()
		loadFeaturedImages()
	}
}

const loadFonts = () => {
	/* <link rel="preconnect" href="https://fonts.gstatic.com">
	 * <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300&display=swap" rel="stylesheet"> */
	const $body = document.querySelector('body')
	const $fontsLoaded = document.createElement('fonts-loaded')
	$fontsLoaded.setAttribute(
		'font-url',
		'https://fonts.googleapis.com/css2?family=Quicksand:wght@300&display=swap'
	)
	$fontsLoaded.setAttribute(
		'font-preload-url',
		'https://fonts.gstatic.com'
	)
	$fontsLoaded.setAttribute(
		'target-element',
		'.Site'
	)
	$fontsLoaded.setAttribute(
		'target-class',
		'is-fontsLoaded'
	)
	$fontsLoaded.setAttribute('hidden', true)
	$body.appendChild($fontsLoaded)
}

const loadImages = () => {
	const $body = document.querySelector('body')
	$body.appendChild(document.createElement('images-loaded'))
}

const loadFeaturedImages = () => {
	const $homeFeaturedImage = document.querySelector('.Home-featuredImage')
	if ($homeFeaturedImage) {
		imagesLoaded($homeFeaturedImage, (instance) => {
			$homeFeaturedImage.classList.add('is-imagesLoaded')
		})
	}

	const $articleFeaturedImage = document.querySelector('.Article-featuredImage')
	if ($articleFeaturedImage) {
		imagesLoaded($articleFeaturedImage, (instance) => {
			$articleFeaturedImage.classList.add('is-imagesLoaded')
		})
	}
}

document.onreadystatechange = documentReady
