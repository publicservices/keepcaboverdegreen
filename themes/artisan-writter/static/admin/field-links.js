export default {
    label: 'Usefull links',
    name: 'links',
    widget: 'list',
    minimal: true,
    buttons: [],
    fields: [
	{
	    label: 'Link',
	    name: 'link',
	    required: false,
	    widget: 'markdown'
	}
    ]
}
